#include <benchmark/benchmark.h>
#include <Eigen/Dense>
#include <array>
#include <cstdlib>

struct FourVector {
    std::array<double, 4> m_mom;
    const double& operator[](size_t i) const { return m_mom[i]; }
};

double Dot(const FourVector &a, const FourVector &b) {
    return a[0]*b[0] - a[1]*b[1] - a[2]*b[2] - a[3]*b[3];
}

static void BM_HandDot(benchmark::State &state) {
    FourVector a{600, 0, 400, 300};
    for(auto _ : state) {
        benchmark::DoNotOptimize(Dot(a, a)); 
    }
}

double EigenDot(const Eigen::Vector4d &a, const Eigen::Vector4d &b) {
    static const Eigen::Vector4d metric{1, -1, -1, -1};
    return a.cwiseProduct(metric.cwiseProduct(b)).sum();
}

static void BM_EigenDot(benchmark::State &state) {
    Eigen::Vector4d a(600, 0, 400, 300);
    for(auto _ : state) {
        benchmark::DoNotOptimize(EigenDot(a, a));
    }
}

BENCHMARK(BM_HandDot);
BENCHMARK(BM_EigenDot);
BENCHMARK_MAIN();
